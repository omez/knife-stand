# Knife Stand

![Image of Yaktocat](images/Stand_2021-Sep-18_07-38-27PM-000_CustomizedView13466997298.png)

Wood thickness:

- `5mm` for legs
- `18mm` for base

Use:
 - [leg.dxf](leg.dxf) - for Leg design
 - [base.dxf](base.dxf) - for Base design
